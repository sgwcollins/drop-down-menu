
function getCategories(data){
	if(data.category == 'top'){
	    return [
	      'Server apple',
	      'Server banana',
	      'Server pear',
	      'Server orange'
	    ];
	}
  	if (data.category == 'additional') {
    	return [
	      'Server square',
	      'Server circle',
	      'Server oval',
	      'Server diamond'
	    ];
  }
  return [];

}

const endpoints = {
	"/categories":{
		"get": getCategories
	}
}

function getFunction(url, data, callback) {
  const domain = url.substring(0, url.indexOf("/"));
  const endpoint = url.substring(url.indexOf("/"), url.length);

  callback(endpoints[endpoint]["get"](data));
}

const api = {
  get: getFunction
};