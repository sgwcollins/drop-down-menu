let HOST = 'server.com/';

function populateCategories(category) {
  api.get(HOST + 'categories', {category}, function(categories) {
    let newCategories = '';
    console.log(categories);
    for (const category of categories) {
      const categoryElement = `
        <li class="menu__sub__categories__item">
          <a href="#" class="menu__sub__categories__item__link">${category}</a>
        </li>
      `;
      newCategories += categoryElement;
    }
    const categoriesElement = document.getElementsByClassName(`menu__sub__categories__items--${category}`)[0];
    console.log(`menu__sub__categories__items--${category}`);
    categoriesElement.innerHTML = newCategories;
  });
}


function showSubmenu() {
  const submenu = document.getElementsByClassName("menu__sub")[0];
  submenu.style.display = "block";

  populateCategories('top');
  populateCategories('additional');
}

function hideSubmenu(){
	const submenu = document.getElementsByClassName("menu__sub")[0];
	submenu.style.display = "none";
}

function menuItemLeave() {
  const submenu = document.getElementsByClassName("menu__sub")[0];
  submenu.style.display = "none";
}


let activeMenuItem = null;

function onMenuItemMouseEnter(item) {
  if (activeMenuItem) {
    activeMenuItem.classList.remove("menu__main__item--active");
  }


  activeMenuItem = item;
  item.classList.add("menu__main__item--active");
  showSubmenu();
}

const menuItems = document.getElementsByClassName("menu__main__item");

for (const menuItem of menuItems) {
  menuItem.onmouseenter = () => onMenuItemMouseEnter(menuItem);
}

function deactivateMenuItem() {
  activeMenuItem.classList.remove("menu__main__item--active");
}

const menu = document.getElementsByClassName("menu")[0];
menu.onmouseleave = hideSubmenu